#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(int argc, char *argv[]){
  // Check if there are the required arguments
  if (argc != 5){
    printf("Error: Argumennya tidak sesuai\n");
    return 1;
  }

  // Get the hour, minute, second
  int jam, menit, detik;
  if (strcmp(argv[1], "*") == 0){
    jam = -1;
  }else{
    jam = atoi(argv[1]);
    if (jam < 0 || jam > 23){
      printf("Error: Invalid hour argument\n");
      return 1;
    }
  }

  if (strcmp(argv[2], "*") == 0){
    menit = -1;
  }else{
    menit = atoi(argv[2]);
    if (menit < 0 || menit > 59)
    {
      printf("Error: Argumennya tidak sesuai\n");
      return 1;
    }
  }

  if (strcmp(argv[3], "*") == 0){
    detik = -1;
  }
  else{
    detik = atoi(argv[3]);
    if (detik < 0 || detik > 59)
    {
      printf("Error: Argumennya tidak sesuai\n");
      return 1;
    }
  }

  // Check the script file
  if (access(argv[4], F_OK) == -1){
    printf("Error: File script tidak ada\n");
    return 1;
  }
  else if (access(argv[4], X_OK) == -1){
    printf("Error: File script tidak bisa di eksekusi\n");
    return 1;
  }

  pid_t pid = fork();
  if (pid < 0){
    exit(EXIT_FAILURE);
  }
  if (pid > 0){
    exit(EXIT_SUCCESS);
  }

  umask(0);

  pid_t sid = setsid();
  if (sid < 0){
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0){
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  // Loop every second
  time_t current_time;
  struct tm *local_time;
  while (1){
    // Get the current local time
    current_time = time(NULL);
    local_time = localtime(&current_time);

    // Check if the current time matches the specified time
    if ((jam == -1 || jam == local_time->tm_hour) && (menit == -1 || menit == local_time->tm_min) && (detik == -1 || detik == local_time->tm_sec)){
      execl("/bin/bash", "/bin/bash", argv[4], NULL);
      exit(0);
    }
    sleep(1);
  }
  return 0;
}