# sisop-praktikum-modul-2-2023-WS-F05

| NAMA  | NRP |
| ------------- | ------------- |
| Azhar Abiyu Rasendriya Harun  | 5025211177  |
| Beauty Valen Fajri  | 5025211227 |
| Ferza Noveri  | 5025211097 |

## NOMOR 1
#### Point A
#### Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut

```shell
int main()
{
download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "binatang.zip");
  unzip("binatang.zip");
  getFile(".");
```

Kode tersebut menunjukkan fungsi `main()` yang memanggil tiga fungsi lainnya seperti `download()`, `unzip()`, dan `getFile()`. Fungsi `download()` dipanggil untuk mengunduh file dari `"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq"` dan menyimpannya dengan nama file `“binatang.zip”`.

Selanjutnya, fungsi `unzip()` dipanggil untuk mengekstrak file zip tersebut. Sedangkan fungsi `getFile()` dipanggil untuk menampilkan daftar file dalam direktori saat ini dengan menggunakan titik "." sebagai parameter.

```shell
generateDirectory();
  moveAnimal(".", "HewanDarat", "HewanAir", "HewanAmphibi");
  zipAnimal();
}
```

Kode tersebut menunjukkan fungsi `generateDirectory()`, `moveAnimal()`, dan `zipAnimal()` yang dipanggil dalam fungsi `main()`.

Fungsi `generateDirectory()` dipanggil untuk membuat beberapa direktori baru. Selanjutnya, fungsi `moveAnimal()` dipanggil untuk memindahkan file-file yang sesuai ke tiga direktori yang telah dibuat sebelumnya. Argumen pertama "." berarti direktori saat ini. Sedangkan fungsi `zipAnimal()` dipanggil untuk mengarsipkan file-file dalam satu direktori menjadi satu file zip.

```shell
void download(char *url, char *namefile)
{
  int stat;
  pid_t downloadID = fork();
  ```

Fungsi `download()` adalah fungsi yang digunakan untuk mengunduh file dari URL yang diinginkan. Pada baris selanjutnya, fungsi tersebut melakukan inisialisasi variabel "stat" dengan tipe data integer. Kemudian, fungsi tersebut menggunakan fungsi `fork()` untuk membuat sebuah proses baru dengan menyimpan nilai pengembalian dari fungsi fork() ke dalam variabel `"downloadID"` yang bertipe data `pid_t`.

```shell
  if (downloadID == 0)
  {
    char *args[] = {"wget", "--no-check-certificate", url, "-q", "-O", namefile, NULL};
```

kode tersebut mendeklarasikan apabila nilai dari variabel `"downloadID"` sama dengan nol maka akan dieksekusi, hal tersebut menandakan bahwa kode sedang berjalan di dalam proses yang baru saja dibuat dengan menggunakan fungsi `fork()`. Selain itu, kode akan menjalankan perintah `"wget"` untuk mendownload file dari URL yang telah diberikan pada parameter "url". 

- "--no-check-certificate" : mengabaikan sertifikat SSL saat mendownload file
- "url" : URL dari file yang akan didownload
- "-q" : menghilangkan output yang ditampilkan ke layar selama proses download berlangsung
- "-O" : menentukan nama file yang akan disimpan sebagai hasil download
- "NULL" : menandakan akhir dari daftar argumen

```shell
execv("/opt/homebrew/bin/wget", args);
  }
   ```
Kemudian, fungsi `"execv"` dipanggil dengan parameter `"/opt/homebrew/bin/wget"` dan `"args"`. Fungsi `"execv"` akan menggantikan kode proses saat ini dengan kode dari perintah `"wget"`. Dengan demikian, proses tersebut akan selesai setelah selesai menjalankan perintah `"wget"` untuk mendownload file tersebut.

```shell
  waitpid(downloadID, &stat, 0);
}
```
Fungsi `waitpid()` digunakan untuk menunggu proses download selesai sebelum memanggil fungsi `unzip()`.

#### Point B
#### Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

```shell
void unzip(char *sourceDir)
{
  int stat;
  pid_t unzipID = fork();
  ```
Fungsi tersebut mendeklarasikan variabel `"stat"` dengan tipe data integer. Kemudian, fungsi tersebut menggunakan fungsi `fork()` untuk membuat sebuah proses baru dengan menyimpan nilai pengembalian dari fungsi `fork()` ke dalam variabel `"unzipID"` yang bertipe data `pid_t`.

  ```shell
if (unzipID == 0)
  {
    char *args[] = {"unzip", "-q", sourceDir, NULL};
    execv("/usr/bin/unzip", args);
  }

  waitpid(unzipID, &stat, 0);
}
 ```
Setelah itu, kode akan memeriksa apakah nilai dari `"unzipID"` sama dengan nol atau tidak. Jika nilai dari `"unzipID"` sama dengan nol, maka kode tersebut sedang berjalan di dalam proses yang baru saja dibuat dengan menggunakan fungsi `fork()` di baris sebelumnya. 

Fungsi `"execv"` akan menggantikan kode proses saat ini dengan kode dari perintah `"unzip"`. Dengan demikian, proses yang diinginkan akan selesai setelah selesai menjalankan perintah "unzip" untuk mengekstrak file dari direktori sumber tersebut.

```shell
void getFile(char *directory)
{
  int stat;
  char *path[100];

  struct dirent *dp;
  DIR *folder;
  srand(time(NULL));
  folder = opendir(directory);
```
Dalam implementasinya, fungsi ini melakukan inisialisasi variabel stat dan membuat sebuah array path yang dapat menampung path dari setiap file. Kemudian, fungsi `opendir()` digunakan untuk membuka direktori yang diberikan pada parameter directory. Setelah direktori terbuka, Setiap file yang terbaca akan diperiksa apakah merupakan sebuah direktori atau bukan. Jika file yang terbaca bukan sebuah direktori, maka path dari file tersebut akan disimpan ke dalam array path.

Setelah selesai iterasi, fungsi `closedir()` digunakan untuk menutup direktori yang telah dibuka sebelumnya dengan fungsi `opendir()`. Sehingga fungsi `getFile()` akan mengembalikan array path yang berisi path dari setiap file yang bukan direktori di dalam direktori yang diberikan pada parameter directory.

```shell
  if (folder != NULL)
  {
    int i = 0;
    while ((dp = readdir(folder)) != NULL)
    {
```
Kode tersebut dideklarasikan untuk melakukan pengecekan apakah folder (yang merupakan pointer ke direktori yang telah dibuka) tidak bernilai `NULL`. Jika folder tidak `NULL`, maka kondisi tersebut akan menjalankan sebuah loop while.

Fungsi `readdir()` digunakan untuk membaca setiap file atau direktori yang terdapat di dalam direktori yang telah dibuka. Setiap file yang terbaca akan disimpan di dalam variabel dp.

```shell
 if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".jpg") != NULL)
      {
```
Kode melakukan pengecekan untuk memastikan bahwa file yang sedang dibaca adalah file gambar dengan ekstensi `".jpg"`. Pertama-tama, kode menggunakan fungsi `strcmp()` untuk membandingkan nama file dengan dua string. Selanjutnya, kode menggunakan fungsi `strstr()` untuk memeriksa apakah nama file mengandung substring `".jpg"`. Fungsi `strstr()` akan mengembalikan pointer ke posisi pertama dari substring yang dicari dalam string yang diberikan. Jika substring ditemukan, maka nilai yang dikembalikan tidak `NULL`.    

```shell
  if (dp->d_type == DT_REG)
        {
          char *token dp->d_name; =
          path[i] = token;
          i++;
        }
      }
    }
```
Kondisi tersebut memastikan bahwa file yang sedang dibaca adalah sebuah file regular dengan menggunakan nilai konstanta `DT_REG` pada variabel `d_type`. Jika file tersebut merupakan sebuah file regular, maka kode akan memperoleh nama file tersebut dari variabel `d_name`. Nama file tersebut akan disimpan ke dalam array path pada interval `i`, dan nilai `i` akan ditambahkan satu untuk menyimpan nama file pada posisi selanjutnya di dalam array path.

```shell
 int size = sizeof(*path);
    int random = rand() % size;
```
Nilai size akan disi dengan ukuran array path dalam byte dan sebuah bilangan acak random akan dihasilkan dengan menggunakan fungsi `rand()` dengan nilai maksimum size. Bilangan acak tersebut akan digunakan untuk memilih nama file secara acak dari array path.

```shell
FILE *file;
    file = fopen("penjaga.txt", "w");
    fprintf(file, "Hewan Yang Dijaga : %s", strtok(path[random], "."));
    fclose(file);
    closedir(folder);
  }
}
```
Setelah selesai memproses setiap file di dalam direktori yang telah dibuka, kode akan mencetak nama file gambar yang terpilih secara acak ke dalam file `"penjaga.txt"`. Kode tersebut mendeklarasikan sebuah pointer ke FILE dengan nama file. Selanjutnya, kode membuka file `"penjaga.txt"` dengan mode `"w"` menggunakan fungsi `fopen()` yang akan menghapus isi file jika file tersebut sudah ada atau membuat file baru jika file belum ada.

Selanjutnya, kode menggunakan fungsi `fprintf()` untuk menulis string ke dalam file yang telah dibuka. String yang ditulis akan berisi informasi mengenai hewan yang dijaga diperoleh dari nama file gambar yang terpilih secara acak. Untuk memperoleh nama hewan, kode menggunakan fungsi `strtok()` untuk membagi nama file menjadi dua bagian. Selanjutnya, bagian pertama dari nama file, yang merupakan nama hewan, akan dicetak ke dalam file `"penjaga.txt"`.

Setelah selesai menulis ke dalam file, kode menutup file menggunakan fungsi `fclose()`. 
Selanjutnya, kode menutup direktori yang telah dibuka menggunakan fungsi `closedir()`.

#### Point C
#### Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

```shell
void generateDirectory()
{
  int stat;
  id_t child_id;

  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanDarat", NULL};
    execv("/bin/mkdir", args);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanAir", NULL};
    execv("/bin/mkdir", args);
  }
  while ((wait(&stat)) > 0)
    ;
 if ((child_id = fork()) == 0)
  {
    char *args[] = {"mkdir", "-p", "HewanAmphibi", NULL};
    execv("/bin/mkdir", args);
  }
  while ((wait(&stat)) > 0)
    ;
```

Fungsi ini akan membuat direktori `"HewanDarat"` menggunakan perintah `mkdir` pada terminal. Kode tersebut mendeklarasikan variabel `child_id ` dengan tipe `id_t`, yang akan menampung nilai `pid` dari proses child yang dibuat menggunakan fungsi `fork()`. 

Setelah itu, kode tersebut mengeksekusi perintah `“mkdir -p HewanDarat”` menggunakan fungsi `execv()`. Setelah proses child selesai dieksekusi, kode menggunakan loop while dengan `wait()` untuk menunggu proses child selesai sebelum melanjutkan eksekusi kode selanjutnya. Hal ini diperlukan agar proses parent tidak mengeksekusi perintah selanjutnya sebelum proses child selesai dieksekusi. 

Kode yang digunakan dalam direktori `“HewanDarat”` tersebut juga sama halnya dengan kode untuk deklarasi kode untuk direktori `“HewanAir”` dan `“HewanAmphibi”`.

“mkdir” : menunjukkan nama perintah yang akan dijalankan
“-p” : menandakan bahwa perintah mkdir akan membuat direktori secara rekursif jika direktori tersebut belum ada

```shell
waitpid(child_id, &stat, 0);
}
```

Kode `"waitpid(child_id, &stat, 0);"` digunakan untuk menunggu proses child dengan id `"child_id"` untuk berhenti dan mengambil status keluaran dari proses child tersebut. Parameter kedua `"&stat"` adalah alamat dari variabel `"stat"` yang akan berisi status keluaran child setelah proses child selesai dieksekusi. Parameter ketiga `"0"` menunjukkan bahwa fungsi `"waitpid"` akan menunggu sampai proses child selesai dan tidak melakukan operasi lain selama menunggu. 

Setelah proses child selesai, fungsi `"waitpid"` akan mengembalikan nilai ke dalam variabel `"stat"`, bisa disimpulkan bahwa kode tersebut bertujuan untuk menunggu proses child selesai dieksekusi sebelum melanjutkan eksekusi kode selanjutnya.

```shell
void moveAnimal(char *soruce, char *des1, char *des2, char *des3)
{
```
Kode `"void moveAnimal(char *source, char *des1, char *des2, char *des3)"` adalah sebuah fungsi yang digunakan untuk memindahkan file atau direktori dari direktori sumber (source) ke salah satu dari tiga direktori tujuan (des1, des2, des3).

```shell
  int stat;
  id_t child_id;
  struct dirent *dp;
  DIR *folder;
  folder = opendir(soruce);
```
Variabel `"stat"` digunakan untuk menyimpan status keluaran dari proses child yang akan dibuat nanti. Variabel `"child_id"` akan digunakan untuk menyimpan `ID` proses child yang akan dibuat. `Struct "dirent"` dan `"DIR"` digunakan untuk membaca isi dari direktori. Fungsi `"opendir"` akan membuka direktori sumber yang akan dipindahkan file atau direktorinya.

```shell
  if (folder != NULL)
  {
    while ((dp = readdir(folder)) != NULL)
    {
      if (dp->d_type == DT_REG)
      {
        // if match with darat
        if (strstr(dp->d_name, "darat") != NULL)
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp->d_name, des1, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
}
        if (strstr(dp->d_name, "air") != NULL)
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp->d_name, des2, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
        if (strstr(dp->d_name, "amphibi") != NULL)
        {
          if ((child_id = fork()) == 0)
          {
            char *argv[] = {"mv", dp->d_name, des3, NULL};
            execv("/bin/mv", argv);
          }
          while ((wait(&stat)) > 0)
            ;
        }
      }
    }
```
Kode ini melakukan looping untuk membaca isi direktori sumber menggunakan fungsi `"readdir"`. Jika isi dari direktori sumber berupa regular file, maka dilakukan pencocokan pada nama file tersebut menggunakan fungsi `"strstr"`. 

Jika nama file tersebut mengandung kata `"darat"`, maka akan dibuat sebuah proses `child` dengan fungsi `"fork"`. Dalam proses child tersebut, file yang terkait dengan proses ini dilakukan perintah `“move”` dari direktori sumber ke direktori tujuan yang ditentukan `(des1)` menggunakan fungsi `"execv"`. Setelah proses child selesai, parent process akan menunggu sampai proses child selesai menggunakan fungsi `"wait"`.

Kode yang digunakan dalam direktori `“HewanDarat”` tersebut juga sama halnya dengan kode untuk deklarasi kode untuk direktori `“HewanAir”` dan `“HewanAmphibi”`.

```shell   
 closedir(folder);
  }
  waitpid(child_id, &stat, 0);
}
```
Kode `"closedir(folder);"` digunakan untuk menutup direktori sumber setelah semua file atau direktori yang ada di dalamnya telah dipindahkan.
Setelah selesai memindahkan semua file atau direktori, kode akan menunggu sampai semua proses child selesai dieksekusi menggunakan fungsi `"waitpid"` dengan parameter `"child_id"` sebagai `ID` dari proses child yang ditunggu. 

Fungsi `"waitpid"` akan mengembalikan status keluaran dari proses anak ke variabel `"stat"`. Parameter ketiga `"0"` menunjukkan bahwa fungsi `"waitpid"` akan menunggu sampai proses anak selesai dan tidak melakukan operasi lain selama menunggu.

#### Point D
#### Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

```shell
void zipAnimal()
{
  id_t child_id;
  int stat;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanAir.zip", "HewanAir", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  if ((child_id = fork()) == 0)
  {
    char *argv[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL};
    execv("/usr/bin/zip", argv);
  }
  while ((wait(&stat)) > 0)
    ;
  waitpid(child_id, &stat, 0);
}
```
Kode tersebut merupakan implementasi fungsi `zipAnimal()` yang berfungsi untuk melakukan kompres `(zip)` pada direktori `HewanDarat`, `HewanAir`, dan `HewanAmphibi` masing-masing menjadi file zip dengan nama `HewanDarat.zip`, `HewanAir.zip`, dan `HewanAmphibi.zip`.

Pada fungsi `"fork"` digunakan untuk membuat sebuah proses child. Dalam proses child, fungsi `"execv"` digunakan untuk menjalankan perintah `"zip"` dengan argumen yang disimpan dalam array `"argv"`. Parameter `"-r"` digunakan untuk melakukan rekursif pada direktori `"HewanDarat"` dan membuat sebuah file zip baru dengan nama `"HewanDarat.zip"`.

Setelah proses child selesai dieksekusi, parent process akan menunggu sampai proses child selesai menggunakan fungsi `"wait"` dan variabel `"stat"` digunakan untuk menyimpan status keluaran dari proses child tersebut.

Kode yang digunakan dalam direktori `“HewanDarat”` tersebut juga sama halnya dengan kode untuk deklarasi kode untuk direktori `“HewanAir”` dan `“HewanAmphibi”`.

## NOMOR 2
#### Point A
#### Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

```shell
char* make_dir(){
   char *now = malloc(sizeof(char) * 20);
   time_t t = time(NULL);
   strftime(now, 20, "%Y-%m-%d_%H:%M:%S", localtime(&t));//buat nama direk, sesuai peraturan
   pid_t child = fork();
```

- Kode tersebut adalah sebuah fungsi yang bernama `make_dir()`. Fungsi ini memiliki dua tujuan utama :
Membuat sebuah string yang berisi nama direktori baru.
Membuat sebuah child process baru menggunakan sistem call fork().
- Pertama-tama, fungsi ini menggunakan `malloc()` untuk mengalokasikan 20 byte dari heap memory. Pointer now menunjuk ke lokasi awal dari blok tersebut. Selanjutnya, fungsi ini mendapatkan waktu saat ini menggunakan fungsi `time()`. Waktu ini disimpan dalam variabel `t`.
- Fungsi `strftime()` digunakan untuk melakukan formatting terhadap waktu saat ini sesuai dengan format yang diinginkan, dan kemudian hasil formatting tersebut disimpan dalam `string now`. Format yang digunakan adalah `"%Y-%m-%d_%H:%M:%S"`, yang berarti `"tahun-bulan-tanggal_jam:menit:detik"`.
- Setelah mendapatkan nama direktori baru, fungsi ini membuat sebuah child process baru menggunakan sistem call `fork()` yang hasilnya nanti disimpan dalam variabel child. Karena child process ini memiliki salinan dari semua variabel dan kode dari parent process, maka child process akan memiliki akses ke nama direktori baru yang sudah di-generate. 

```shell
if(child == 0){
       char *cmd[] = {"mkdir", now, NULL};
       execv("/bin/mkdir", cmd);//execute mkdir
   }
   while(wait(NULL) != child);
   return now;//return nama dir
}
```

- Setelah child process berhasil dibuat dengan menggunakan `fork()`, fungsi `make_dir()` melakukan pengecekan apakah proses yang sedang berjalan saat ini adalah child process atau bukan. Jika proses yang sedang berjalan adalah child process (ditandai dengan nilai child == 0), maka fungsi ini akan melakukan langkah-langkah berikut:
Membuat sebuah array `cmd` yang berisi argumen-argumen untuk perintah mkdir. Array ini terdiri dari string `"mkdir"` (nama perintah), `now` (nama direktori baru yang sudah di-generate sebelumnya), dan `NULL` (penanda akhir dari array).
- Menggunakan sistem call `execv()` untuk menjalankan perintah `mkdir` dan mengirimkan argumen-argumen yang sudah disiapkan. `"/bin/mkdir"` adalah path menuju executable dari perintah mkdir.
- Jika proses yang sedang berjalan adalah parent process (ditandai dengan nilai child yang bukan 0), maka fungsi ini akan menunggu hingga child process selesai dijalankan menggunakan sistem call `wait()`. Setelah child process selesai dijalankan, parent process akan mengembalikan nilai now, yaitu string yang berisi nama direktori baru yang sudah di-generate sebelumnya.

```shell
void start_program(char *dir) {
   char filename[64];
   char url[64];
   char location[100];
   for(int i = 0; i < 15; i++){
       pid_t child = fork();
```
- Kode tersebut merupakan implementasi dari fungsi `start_program()` yang menerima parameter `dir` yang merupakan `path` dari direktori tujuan. Fungsi ini akan melakukan loop sebanyak 15 kali dan pada setiap iterasi akan membuat sebuah child process baru menggunakan sistem call `fork()`.
- Selanjutnya, kode akan mengeksekusi program yang diinginkan pada setiap iterasi. Di dalam kode tersebut terdapat 3 variabel yang digunakan untuk menampung `nama file`, `URL`, dan `lokasi` (digunakan untuk menentukan direktori tujuan dari file yang diunduh).

#### Point B
#### Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].
```shell
if(child == 0){
           time_t t = time(NULL);  // dapatkan waktu terkini dalam detik, Epoch
           long size = (long)t % 1000 + 50;  // hitung besar gambar
           strftime(filename, 64, "%Y-%m-%d_%H:%M:%S.png", localtime(&t));//filename
           sprintf(location, "%s/%s", dir, filename);  // spesifikasi output
           sprintf(url, "https://picsum.photos/%ld", size);
```
- Dapatkan url, dari perhitungan kode tersebut adalah bagian dari loop dalam fungsi `start_program()`. Loop tersebut berisi implementasi yang bertujuan untuk mengunduh file gambar dari internet menggunakan program `wget`.
- Kode melakukan pengecekan apakah process saat ini merupakan child process atau bukan dengan mengecek nilai variabel child. Jika nilai child adalah 0, maka kode tersebut berada dalam child process, dan proses selanjutnya adalah mengunduh sebuah file gambar dari internet dengan format yang diinginkan soal.
- Selanjutnya, kode membuat sebuah string url yang berisi URL dari file gambar yang akan diunduh. Ukuran file gambar ditentukan oleh variabel size, yang dihitung dengan menggunakan waktu saat ini dalam detik dan kemudian diambil modulo 1000 dan ditambahkan 50.
```shell
 char *cmd[] = {"wget", "-q", "-O", location, url, NULL};
           execv("/usr/bin/wget", cmd);//execute wget
       }
       sleep(5);//jeda 5 detik
   }
```
- Kode selanjutnya adalah membuat sebuah array of string `cmd` yang berisi argumen-argumen yang akan digunakan saat menjalankan perintah `wget` sepertihalnya nama program `(wget)`, opsi `-q` untuk menjalankan wget secara quiet (tanpa output), opsi `-O` untuk menentukan nama file output, dan `URL` dari file yang akan diunduh. Setelah itu, perintah `wget` dijalankan menggunakan `execv()`.
- Setelah berhasil mendownload file gambar, kode melakukan jeda selama 5 detik menggunakan fungsi `sleep()`. Hal ini dilakukan untuk memastikan bahwa proses download tidak terlalu cepat dan memberikan waktu yang cukup untuk proses download sebelum proses berikutnya dimulai.

#### Point C 
#### Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).
```shell
har zipName[30]; //nama zip
    sprintf(zipName, "%s.zip", dir);//masukkan dir menajdi nama zip
    pid_t child = fork();//child
    if(child == 0){
        char *cmd1[] = {"zip", "-r", zipName, dir, NULL};
        execv("/usr/bin/zip", cmd1);//execute zip -r
    }
```
- `execv("/usr/bin/zip", cmd1);//execute zip -r` melakukan zip terhadap folder yang sudah terisi 15 gambar.

#### Point D
#### Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.
```shell
void generate_killer(char *argv[], pid_t pid, pid_t sid) {
    FILE *fp = fopen("killer.c", "w");//buat file killer.c
    // Killer.c
    const char *command = ""
    "#include <unistd.h>\n"
    "#include <sys/wait.h>\n"
    "int main() {\n"
        "pid_t child = fork();\n"
        "if (child == 0) {\n"
            "%s"
            "execv(\"%s\", argv);\n"
        "}\n"
        "while(wait(NULL) > 0);\n"
        "child = fork();\n"
        "if (child == 0) {\n"
        "char *argv[] = {\"rm\", \"killer.c\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"//hapus killer.c
        "}\n"
        "while(wait(NULL) > 0);\n"
        "char *argv[] = {\"rm\", \"killer\", NULL};\n"
        "execv(\"/bin/rm\", argv);\n"//self destruct
    "}\n";
    // Mode A
    char cmd[1234];
    if (strcmp(argv[1], "-a") == 0) {
        sprintf(cmd, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
        fprintf(fp, command, cmd, "/usr/bin/pkill");//jika argumen -a, pkill
    }
    // Mode B
    if (strcmp(argv[1], "-b") == 0) {
        sprintf(cmd, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
        fprintf(fp, command, cmd, "/bin/kill");//jika argumen -b, kill
    }
    fclose(fp);
    // Compile killer.c
    pid = fork();
    if(pid == 0){    
        char *cmd[] = {"gcc", "killer.c", "-o", "killer", NULL};
        execv("/usr/bin/gcc", cmd);//execute gcc, compile killer.c
    }
    while(wait(NULL) != pid);
}
```

#### Point E 
#### Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
```shell
char cmd[1234];
    if (strcmp(argv[1], "-a") == 0) {
        sprintf(cmd, "char *argv[] = {\"pkill\", \"-9\", \"-s\", \"%d\", NULL};\n", sid);
        fprintf(fp, command, cmd, "/usr/bin/pkill");//jika argumen -a, pkill
    }
    // Mode B
    if (strcmp(argv[1], "-b") == 0) {
        sprintf(cmd, "char *argv[] = {\"kill\", \"-9\", \"%d\", NULL};\n", getpid());
        fprintf(fp, command, cmd, "/bin/kill");//jika argumen -b, kill
    }
```

## NOMOR 3
#### Point A
#### Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

```shell
int main()
{ 
  download("https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", "players.zip");
  unzip("players.zip");
```
Di dalam fungsi download tersebut menunjukkan bahwa kode dideklarasikan untuk mengunduh file dari alamat URL yang diberikan dan dalam hal ini file yang diunduh adalah `"players.zip"`.

Sedangkan perintah `unzip` berguna untuk mengekstrak isi dari file `"players.zip"`.

```shell
void donwload(char *url, char *namefile)
{
  int stat;
  pid_t downloadID = fork();
```
Kode `void download(char *url, char *namefile)` merupakan kode yang bertujuan untuk mengunduh file dari URL yang diberikan dan menyimpannya dengan nama file yang diberikan.

Kemudian `int stat;` deklarasi yang digunakan untuk menyimpan status hasil dari proses unduhan. Selanjutnya `pid_t downloadID = fork();` digunakan untuk membuat proses baru. Fungsi `fork()` akan mengembalikan pid dari proses yang dibuat sedangkan Variabel downloadID digunakan untuk menyimpan pid dari proses baru yang dibuat.

```shell
  if (downloadID == 0)
  {
    char *args[] = {"wget", "--no-check-certificate","-q", url, "-O", namefile, NULL};
    // ! Note: /urs/local/bin/wget is the path to wget in MacOS change it to /usr/bin/wget if you are using Linux
``` 
 
Kode tersebut adalah pernyataan kondisional yang melakukan pengecekan apakah program sedang berjalan atau tidak. Kondisi ini terpenuhi ketika nilai `downloadID` sama dengan `0`, yang berarti program sedang berjalan.

Untuk kode `char *args[] = {"wget", "--no-check-certificate","-q", url, "-O", namefile, NULL};` adalah array karakter yang berisi argumen yang digunakan pada system `execv()`. Dalam kode tersebut berisi perintah untuk melakukan unduhan file dari URL yang diberikan (url) dan menyimpannya dengan nama file yang diberikan (namefile). Opsi `--no-check-certificate` akan menonaktifkan pemeriksaan sertifikat SSL, sedangkan opsi `-q` akan menjalankan `wget` dalam mode diam-diam (quiet mode).

```shell
execv("/usr/bin/wget", args);
  }
```

Kode yang akan mengeksekusi perintah `"wget"` dengan argumen yang telah ditentukan sebelumnya. System call `execv()` akan menggantikan proses yang sedang berjalan dengan proses yang baru dieksekusi.

```shell
 waitpid(downloadID, &stat, 0);
}
```

Kode yang digunakan untuk menunggu proses selesai dieksekusi. Pada parameter pertama, `waitpid()` menerima pid dari proses yang ditunggu, yaitu `downloadID`. Parameter kedua `&stat` adalah alamat dari variabel stat yang digunakan untuk menyimpan status hasil dari proses unduhan. Parameter ketiga `0` menandakan bahwa `waitpid()` akan berjalan dalam mode default.

```shell
void unzip(char *sourceDir)
{
  int stat;
  pid_t unzipID = fork();

  if (unzipID == 0)
  {
    char *args[] = {"unzip", "-q", sourceDir, "-d", ".", NULL};
    execv("/usr/bin/unzip", args);
  }
  while (wait(&stat) > 0)
    ;
```

Kode tersebut merupakan bagian dari fungsi `unzip` yang memiliki satu parameter yaitu `sourceDir`. Pernyataan kondisional yang digunakan untuk melakukan pengecekan apakah program sedang berjalan atau tidak. Kondisi ini terpenuhi ketika nilai `unzipID` sama dengan `0`, yang berarti program sedang berjalan.

Untuk kode `while (wait(&stat) > 0) ;` merupakan kode perulangan yang berjalan selama fungsi `wait()` mengembalikan nilai lebih dari `0`. Fungsi `wait()` digunakan untuk menunggu proses selesai dieksekusi.

```shell
  unzipID = fork();
  if (unzipID == 0)
  {
    char *args[] = {"rm", "-f", sourceDir, NULL};
    execv("/bin/rm", args);
  }
  while (wait(&stat) > 0)
    ;

  waitpid(unzipID, &stat, 0);
}
```

Kode tersebut adalah bagian dari fungsi unzip yang digunakan untuk menghapus file zip setelah diekstrak. Untuk kode `pid_t unzipID = fork();` adalah kode yang nantinya akan digunakan untuk menjalankan perintah `rm`.

Kemudian `if (unzipID == 0)` adalah pernyataan kondisional yang mengecek apakah program sedang berjalan atau tidak. Kondisi ini terpenuhi ketika nilai `unzipID` sama dengan `0`, yang berarti program sedang berjalan.

Untuk kode `while (wait(&stat) > 0) ;` merupakan kode perulangan yang berjalan selama fungsi `wait()` mengembalikan nilai lebih dari `0` dan untuk kode `waitpid(unzipID, &stat, 0);` adalah system call `waitpid()` yang menunggu proses dengan `ID unzipID` selesai dieksekusi. 

#### Point B
#### Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.

```shell
void removeNotMU(char *directory)
{
  int stat;
  char *path;
  id_t child_id;
  struct dirent *dp;
```
Kode tersebut merupakan kode yang menerima sebuah argumen berupa string yang merepresentasikan direktori tempat file akan dihapus. Untuk kode `char *path;` mendeklarasikan variabel path dengan tipe data pointer ke karakter yang digunakan untuk menyimpan jalur lengkap dari sebuah file atau direktori. Sedangkan kode `struct dirent *dp;` Mendeklarasikan variabel dp dengan tipe data struct dirent, yang akan digunakan untuk membaca isi dari sebuah direktori.

```shell 
DIR *folder;
  folder = opendir(directory);
  if (folder != NULL)
  {
```
Kode `DIR *folder;` dideklarasikan untuk variabel folder dengan tipe data pointer ke tipe data `DIR`. Variabel ini akan digunakan untuk menyimpan informasi tentang direktori yang akan dihapus file-filenya. Sedangkan kode `if (folder != NULL)` dideklarasikan untuk melakukan pengecekan apakah direktori berhasil dibuka atau tidak.

```shell   
 int i = 0;
    while ((dp = readdir(folder)) != NULL)
    {
```
Kode tersebut di deklarasikan untuk variabel `i` dengan nilai awal `0`, yang nantinya akan digunakan sebagai counter. Dan melakukan perulangan pada setiap file dan direktori yang terdapat pada direktori yang dibuka sebelumnya. Untuk fungsi `readdir()` akan mengembalikan nilai `NULL` ketika sudah mencapai akhir dari isi direktori.

```shell 
 if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL)
      {
```
Kode tersebut melakukan pengecekan apakah file yang sedang diproses merupakan file gambar dengan ekstensi `.png` dan bukan merupakan file dengan nama `"."` atau `".."`. 
- Fungsi strcmp() digunakan untuk membandingkan string
- Fungsi strstr() digunakan untuk mencari substring dalam sebuah string.

```shell
if (dp->d_type == DT_REG)
{
```
Kode yang digunakan untuk melakukan pengecekan apakah file yang sedang diproses merupakan file reguler (bukan direktori atau link).

```shell
if (strstr(dp->d_name, "ManUtd") == NULL)
{
```
Kode yang digunakan untuk melakukan pengecekan apakah file yang sedang diproses bukan merupakan file gambar dengan logo klub sepakbola Manchester United, yang ditandai dengan nama file yang mengandung substring "ManUtd".

```shell
path = malloc(strlen(directory) + strlen(dp->d_name) + 2);
sprintf(path, "%s/%s", directory, dp->d_name);
```
Kode untuk mengalokasikan memori untuk variabel path, yang akan digunakan untuk menyimpan path lengkap dari file yang akan dihapus lalu mengisi nilai pada variabel path dengan path lengkap dari file yang akan dihapus.

```shell
child_id = fork();
if (child_id == 0)
{
```
Membuat proses baru dengan menggunakan fungsi `fork()` dan menyimpan `ID` proses baru tersebut pada variabel `child_id` dan menjalankan perintah penghapusan file pada proses baru yang telah dibuat.

```shell
              char *args[] = {"rm", "-f", path, NULL};
              execv("/bin/rm", args);
            }
          }
        }
      }
    }
  }
  closedir(folder);
  waitpid(child_id, &stat, 0);
}
```
Kode tersebut digunakan untuk mendefinisikan argumen-argumen yang akan digunakan pada perintah rm, yaitu path dari file yang akan dihapus dan juga enjalankan perintah rm dengan argumen yang telah ditentukan sebelumnya. Fungsi `execv()` digunakan untuk menjalankan perintah pada proses yang sedang berjalan.

#### Point C 
#### Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.
```shell
void moveByRole(){
  int stat;
  char *path;
  pid_t child_id[4]; /* untuk mendeklarasikan variabel child_id yang bertipe data pid_t dan memiliki ukuran array 4 */
  struct dirent *dp; /* untuk mendeklarasikan variabel dp yang bertipe data struct dirent */
  DIR *folder; /* untuk mendeklarasikan variabel folder yang bertipe data DIR */

  for (int i = 0; i < 4; i++)
  {
    char *newFolder;
    switch (i)
    {
    case 0:
      newFolder = "Bek";
      break;
    case 1:
      newFolder = "Gelandang";
      break;
    case 2:
      newFolder = "Kiper";
      break;
    case 3:
      newFolder = "Penyerang";
      break;
    }
    child_id[i] = fork(); /* untuk melakukan fork pada proses parent */
    if (child_id[i] < 0) /* untuk melakukan pengecekan apakah variabel child_id bernilai kurang dari 0 atau tidak, kurang dari 0 berarti proses tersebut gagal dibuat */
    {
      printf("Gagal membuat child process.\n");
      exit(EXIT_FAILURE);
    }
    else if (child_id[i] == 0) /* untuk melakukan pengecekan apakah variabel child_id bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child */
    {
      char *args[] = {"mkdir", "-p", newFolder, NULL}; /* untuk mendeklarasikan variabel args yang bertipe data char dan memiliki isi array mkdir, -p, newFolder, dan NULL */
      execv("/bin/mkdir", args); /* untuk melakukan eksekusi fungsi mkdir dengan argumen -p dan newFolder */
    }

    while (wait(&stat) > 0) /* untuk melakukan iterasi selama proses child belum selesai */
      ;

    if (folder != NULL) /* untuk melakukan pengecekan apakah variabel folder bernilai NULL atau tidak, NULL berarti proses tersebut gagal dibuat */
    {
      folder = opendir("players"); /* untuk membuka folder players */
      while ((dp = readdir(folder)) != NULL) /* untuk melakukan iterasi selama proses membaca folder players belum selesai */
      {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, ".png") != NULL) /* untuk melakukan pengecekan apakah nama file yang dibaca merupakan file yang memiliki ekstensi .png atau bukan */
        {
          if (dp->d_type == DT_REG && strstr(dp->d_name, newFolder) != NULL) /* untuk melakukan pengecekan apakah nama file yang dibaca merupakan file yang memiliki ekstensi .png dan memiliki nama folder yang sama dengan nama folder yang akan dibuat */
          {
            path = malloc(strlen("players") + strlen(dp->d_name) + 2); /*  untuk melakukan alokasi memori pada variabel path dengan ukuran memori yang dibutuhkan sesuai dengan panjang string players ditambah panjang string dp->d_name ditambah 2 */
            sprintf(path, "%s/%s", "players", dp->d_name); /* untuk melakukan penulisan pada variabel path dengan format string players/dp->d_name */
            child_id[i] = fork(); /* untuk melakukan fork pada proses parent */
            if (child_id[i] == 0) /* untuk melakukan pengecekan apakah variabel child_id bernilai 0 atau tidak, 0 berarti proses tersebut merupakan proses child */
            {
              char *args[] = {"mv", "-f", path, newFolder, NULL}; /* untuk mendeklarasikan variabel args yang bertipe data char dan menginisialisasikan variabel tersebut dengan argumen yang akan digunakan untuk menjalankan program mv */
              execv("/bin/mv", args); /* untuk melakukan eksekusi fungsi mv dengan argumen -f, path, dan newFolder */
            }
          }
        }
      }
    }
  }
  for (int i = 0; i < 4; i++)
  {
    waitpid(child_id[i], &stat, 0); /*  untuk menunggu proses child selesai */
  }
  closedir(folder); /*  untuk menutup folder */
}
```

#### Point D 
#### Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
```shell
void buatTim(int bek, int gelandang, int penyerang){
  int stat;
  pid_t child_id;

  char args[200];
  child_id = fork();

  // Remove file if exist
  if (child_id == 0){
    snprintf(args, sizeof(args), "rm -f ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }
  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Bek | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", bek, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }
  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Gelandang | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", gelandang, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Penyerang | sort -t _ -k4,4 -n -r | head -n %d >> ~/Formasi_%d_%d_%d.txt", penyerang, bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  while(wait(&stat) > 0);

  child_id = fork();
  if (child_id == 0)
  {
    snprintf(args, sizeof(args), "ls ./Kiper | sort -t _ -k4,4 -n -r | head -n 1 >> ~/Formasi_%d_%d_%d.txt", bek, gelandang, penyerang);
    execlp("/bin/bash", "bash", "-c", args, NULL);
  }

  waitpid(child_id, &stat, 0);
}
```

## NOMOR 4
#### Point B
#### Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
#### Point C
#### Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
#### Point D 
#### Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

```shell
if (argc != 5){
    printf("Error: Argumennya tidak sesuai\n");
    return 1;
  }
```
- adalah sebuah kondisi yang memeriksa apakah jumlah argumen yang diberikan sesuai dengan yang diharapkan (yaitu 4 argumen, yaitu jam, menit, detik, dan nama file script yang akan dijalankan). Jika tidak, program akan mengeluarkan pesan kesalahan dan keluar dengan kode 1.

```shell
int jam, menit, detik;
  if (strcmp(argv[1], "*") == 0){
    jam = -1;
  }else{
    jam = atoi(argv[1]);
    if (jam < 0 || jam > 23){
      printf("Error: Invalid hour argument\n");
      return 1;
    }
  }
```
- `int jam, menit, detik`; mendeklarasikan tiga variabel integer yang akan digunakan untuk menyimpan waktu yang ditentukan.
- `if (strcmp(argv[1], "*") == 0){...}` adalah sebuah kondisi yang memeriksa apakah argumen pertama (jam) adalah karakter '*' (yang berarti tidak ada jam tertentu yang ditentukan). Jika ya, variabel jam akan diatur menjadi -1. Jika tidak, variabel jam akan diatur dengan nilai yang sesuai dengan argumen tersebut. Jika argumen tersebut tidak valid (yaitu kurang dari 0 atau lebih besar dari 23), program akan mengeluarkan pesan kesalahan dan keluar dengan kode 1.

```shell
  if (strcmp(argv[2], "*") == 0){
    menit = -1;
  }else{
    menit = atoi(argv[2]);
    if (menit < 0 || menit > 59)
    {
      printf("Error: Argumennya tidak sesuai\n");
      return 1;
    }
  }
```
- `if (strcmp(argv[2], "*") == 0){...}` adalah sebuah kondisi yang memeriksa apakah argumen kedua (menit) adalah karakter '*' (yang berarti tidak ada menit tertentu yang ditentukan). Jika ya, variabel menit akan diatur menjadi -1. Jika tidak, variabel menit akan diatur dengan nilai yang sesuai dengan argumen tersebut. Jika argumen tersebut tidak valid (yaitu kurang dari 0 atau lebih besar dari 59), program akan mengeluarkan pesan kesalahan dan keluar dengan kode 1.

```shell
if (strcmp(argv[3], "*") == 0){
    detik = -1;
  }
  else{
    detik = atoi(argv[3]);
    if (detik < 0 || detik > 59)
    {
      printf("Error: Argumennya tidak sesuai\n");
      return 1;
    }
  }
```
- `if (strcmp(argv[3], "*") == 0){...}` adalah sebuah kondisi yang memeriksa apakah argumen kedua (detik) adalah karakter '*' (yang berarti tidak ada detik tertentu yang ditentukan). Jika ya, variabel detik akan diatur menjadi -1. Jika tidak, variabel detik akan diatur dengan nilai yang sesuai dengan argumen tersebut. Jika argumen tersebut tidak valid (yaitu kurang dari 0 atau lebih besar dari 59), program akan mengeluarkan pesan kesalahan dan keluar dengan kode 1.

```shell
  if (access(argv[4], F_OK) == -1){
    printf("Error: File script tidak ada\n");
    return 1;
  }
  else if (access(argv[4], X_OK) == -1){
    printf("Error: File script tidak bisa di eksekusi\n");
    return 1;
  }
```
- Pertama, `access(argv[4], F_OK)` digunakan untuk mengecek apakah file script yang ditunjuk oleh `argv[4]` ada atau tidak dengan menggunakan parameter `F_OK`. Jika file tersebut tidak ditemukan, maka akan muncul pesan error `"Error: File script tidak ada"` dan program akan keluar dengan nilai `return 1`.
- Kedua, `access(argv[4], X_OK)` digunakan untuk mengecek apakah file script tersebut dapat dieksekusi dengan menggunakan parameter `X_OK`. Jika file tersebut tidak dapat dieksekusi, maka akan muncul pesan error `"Error: File script tidak bisa di eksekusi"` dan program akan keluar dengan nilai `return 1`.

```shell
pid_t pid = fork();
  if (pid < 0){
    exit(EXIT_FAILURE);
  }
  if (pid > 0){
    exit(EXIT_SUCCESS);
  }
```
- Kode tersebut merupakan bagian dari proses untuk mengubah program menjadi daemon. `fork()` adalah fungsi untuk membuat proses baru yang disebut child process yang akan mengeksekusi kode yang sama dengan parent process (program utama). Setelah proses baru terbentuk, parent process akan mengembalikan nilai pid dari child process sedangkan child process akan mengembalikan nilai 0.
- Dalam kode tersebut, nilai pid dari child process yang baru terbentuk disimpan pada variabel `pid`. Dalam penggunaannya, pertama-tama dilakukan pengecekan apakah nilai pid tersebut lebih kecil dari 0. Jika iya, maka proses pembuatan child process gagal dan program akan keluar dengan exit status `EXIT_FAILURE`.
- Selanjutnya, dilakukan pengecekan apakah nilai pid tersebut lebih besar dari 0. Jika iya, maka proses pembuatan child process berhasil dan parent process (program utama) akan keluar dengan exit status `EXIT_SUCCESS`, sementara child process akan berjalan dan mengeksekusi kode yang ada di bawahnya sebagai daemon. Hal ini dilakukan agar program utama tidak mengikuti dan mempengaruhi proses child process yang sedang berjalan. Sehingga program tetap berjalan walaupun proses utama telah selesai dijalankan.

```shell
umask(0);
```
- `umask(0)` digunakan untuk mengatur file mode creation mask ke nilai 0, sehingga saat program berjalan dan membuat file baru, file tersebut akan memiliki permission atau hak akses yang paling lengkap (yaitu, dapat dibaca, ditulis, dan dieksekusi oleh pemilik, grup, dan pengguna lainnya)

```shell
pid_t sid = setsid();
  if (sid < 0){
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0){
    exit(EXIT_FAILURE);
  }
```
- Dalam kode tersebut, `pid_t sid = setsid()` digunakan untuk membuat sesi baru dan menyimpan ID sesi baru ke variabel sid
- Jika `setsid()` mengembalikan nilai kurang dari nol, artinya gagal membuat sesi baru, maka program akan keluar dengan kode keluaran `EXIT_FAILURE`.
- Dalam kode tersebut, if `((chdir("/")) < 0)` digunakan untuk mengubah direktori kerja ke root direktori. Jika `chdir()` mengembalikan nilai kurang dari 0, artinya gagal mengubah direktori kerja, maka program akan keluar dengan kode keluaran `EXIT_FAILURE`.

```shell
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
```
- `close(STDIN_FILENO)`, `close(STDOUT_FILENO)`, dan `close(STDERR_FILENO)` adalah tiga sistem panggilan (system call) yang digunakan dalam sistem operasi UNIX atau Linux untuk menutup file descriptor standar yang terbuka pada proses.
- STDIN_FILENO merujuk pada file descriptor standar untuk input (biasanya keyboard).
- STDOUT_FILENO merujuk pada file descriptor standar untuk output (biasanya terminal atau konsol).
- STDERR_FILENO merujuk pada file descriptor standar untuk output error (biasanya juga ke terminal atau konsol).
- Ketika ketiga panggilan sistem ini dipanggil, maka file descriptor standar yang terbuka akan ditutup. Hal ini biasanya dilakukan dalam daemon atau proses yang berjalan sebagai background, di mana tidak ada interaksi langsung dengan pengguna dan proses tersebut tidak memerlukan file descriptor standar.

```shell
time_t current_time;
  struct tm *local_time;
  while (1){
    // Get the current local time
    current_time = time(NULL);
    local_time = localtime(&current_time);

    // Check if the current time matches the specified time
    if ((jam == -1 || jam == local_time->tm_hour) && (menit == -1 || menit == local_time->tm_min) && (detik == -1 || detik == local_time->tm_sec){
      execl("/bin/bash", "/bin/bash", argv[4], NULL);
      exit(0);
    }
    sleep(1);
  }
```
- Pada baris pertama, terdapat deklarasi variabel current_time dengan tipe time_t, yang digunakan untuk menyimpan waktu saat ini dalam format time_t. Format time_t adalah integer yang merepresentasikan waktu dalam detik sejak 1 Januari 1970 (biasa disebut dengan epoch time).
- Selanjutnya, pada baris kedua, variabel `local_time` dideklarasikan dengan tipe `struct tm*`, yang akan digunakan untuk menyimpan waktu lokal berdasarkan `current_time`. Struktur `tm` menyimpan informasi waktu seperti tahun, bulan, hari, jam, menit, dan detik.
- Setelah itu, program masuk ke dalam sebuah loop yang berjalan terus menerus `(while(1))`. Pada setiap iterasi, program akan mengambil waktu lokal saat ini menggunakan fungsi `time(NULL)`, yang mengembalikan nilai waktu saat ini dalam format `time_t`. Kemudian, waktu lokal ini dikonversi ke dalam bentuk `struct tm` menggunakan fungsi `localtime(&current_time)`, dan disimpan ke variabel `local_time`.
- Selanjutnya, program memeriksa apakah waktu saat ini sama dengan waktu yang di-spesifikasikan pada argumen jam, menit, dan detik yang diberikan pada program. Jika waktu saat ini sama dengan waktu yang di-spesifikasikan, maka program menjalankan perintah yang di-spesifikasikan pada argumen keempat menggunakan fungsi `execl()`. Fungsi `execl()` digunakan untuk menjalankan program baru dari dalam program yang sedang berjalan. Jika perintah tersebut telah berhasil dijalankan, program keluar dari loop menggunakan fungsi `exit(0)`.
- Jika waktu saat ini belum sama dengan waktu yang di-spesifikasikan, maka program akan tidur selama 1 detik menggunakan fungsi `sleep(1)`, sebelum kembali melakukan loop untuk memeriksa waktu saat ini lagi.

